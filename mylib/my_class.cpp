/*! @file my_class.cpp
@author liu yabin
@date 2019/05/14
@copydoc copyright
@brief Implement of MyClass
*/

#include "mylib/my_class.h"

namespace my_ns {
void MyClass::my_print2() {
    std::cout << "print2" << std::endl;
    }
}  // namespace my_ns
