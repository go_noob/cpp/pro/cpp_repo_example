/*! @file my_class.h
@author liu yabin
@date 2019/05/14
@copydoc copyright
@brief define MyClass
*/

#ifndef MYLIB_MY_CLASS_H_
#define MYLIB_MY_CLASS_H_

#include <iostream>

namespace my_ns {
class MyClass {
 public:
    MyClass() {}
    ~MyClass() {}

    // method
    void my_print1() { std::cout << "print1" << std::endl; }
    void my_print2();
};
}  // namespace my_ns

#endif  // MYLIB_MY_CLASS_H_
