/*! @file test_my_class.cpp
@author liu yabin
@date 2019/05/14
@copydoc copyright
@brief Test for MyClass
*/

#include <iostream>

#include "mylib/my_class.h"

int main(int argc, char const *argv[]) {
    my_ns::MyClass my_obj;
    my_obj.my_print1();
    std::cout << "All tests are succeed." << std::endl;
    return 0;
}
