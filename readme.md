# 最终版本

[![pipeline status](https://gitlab.com/go_noob/cpp/pro/cpp_repo_example/badges/master/pipeline.svg)](https://gitlab.com/go_noob/cpp/pro/cpp_repo_example/commits/ci)
[![coverage report](https://gitlab.com/go_noob/cpp/pro/cpp_repo_example/badges/master/coverage.svg)](https://go_noob.gitlab.io/cpp/pro/cpp_repo_example)
[![wiki](https://img.shields.io/badge/-wiki-orange.svg)](https://gitlab.com/go_noob/cpp/pro/cpp_repo_example/wikis/home)


该版本演示了 GitLab CI 的配置。具体介绍请参考对应的 wiki 介绍。