#!/bin/bash
set -e
sed "s%CI_PROJECT_DIR%${CI_PROJECT_DIR}%g" ./Doxyfile_ci > ./Doxyfile_temp
doxygen Doxyfile_temp
rm -f ./Doxyfile_temp