|       文件        |  运行环境  |                 作用                  |
| :---------------: | :--------: | :-----------------------------------: |
|    *check.bat*    |  windows   |       对代码进行风格和静态检查        |
|   *coverage.sh*   | WSL, Linux |     进行测试并生成代码覆盖率报告      |
|    *Doxyfile*     |  不可执行  | windows 环境下定义的 Doxygen 配置文件 |
|   *Dockerfile*    |  不可执行  |        定义了本项目的容器环境         |
| *cpplint_run.sh*  | WSL, Linux |     容器中进行代码风格检查的脚本      |
| *cppcheck_run.sh* | WSL, Linux |     容器中进行代码静态检查的脚本      |
|   *Doxyfile_ci*   |  不可执行  |       容器中的 Doxygen 配置文件       |
| *doxygen_run.sh*  | WSL, Linux |        容器中进行文档自动生成         |

