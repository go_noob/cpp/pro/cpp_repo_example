#!/bin/bash
set -e
root=".."
cpplint --repository=${root} --linelength=100 ${root}/mylib/my_class.cpp
cpplint --repository=${root} --linelength=100 ${root}/mylib/my_class.h
cpplint --repository=${root} --linelength=100 --recursive ${root}/tests