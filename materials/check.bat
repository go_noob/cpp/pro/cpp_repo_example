echo off
set root=..
echo cpplint check
cpplint --repository=%root% --linelength=100 %root%/mylib/my_class.cpp
cpplint --repository=%root% --linelength=100 %root%/mylib/my_class.h
cpplint --repository=%root% --linelength=100 --recursive %root%/tests

echo cppcheck
cppcheck --enable=style,warning,performance,portability -i%root%/mylib/3rd_part %root%/mylib
cppcheck --enable=style,warning,performance,portability %root%/tests