#!/bin/bash
pwd
cd ..
root_path=$(pwd)
echo $root_path
if [ $# -ge 1 ]; then
    if [ $1 = clear ]
    then
        rm -r coverage_builds
    fi
fi
mkdir -p coverage_builds
cd coverage_builds
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=../install -DCodeCoverage=ON ..
make -j 4
echo "start run tests"
ctest
for f in $(find ./ -name '*.gcno'); do
    echo $f
    gcov -p $f >/dev/null 2>&1
done
lcov --rc lcov_branch_coverage=1 -c -d . -o temp >/dev/null 2>&1
lcov --rc lcov_branch_coverage=1 -e temp '*cpp_repo_example*' -o temp2 >/dev/null 2>&1
lcov --rc lcov_branch_coverage=1 -r temp2 '*3rd_part*' -o temp3 >/dev/null 2>&1
lcov --rc lcov_branch_coverage=1 -r temp3 '*tests*' -o temp4 >/dev/null 2>&1
genhtml --rc genhtml_branch_coverage=1 -p $root_path temp4 -o html