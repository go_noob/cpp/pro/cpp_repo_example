#!/bin/bash
set -e
root=".."
cppcheck --enable=style,warning,performance,portability -i${root}/mylib/3rd_part ${root}/mylib
cppcheck --enable=style,warning,performance,portability ${root}/tests